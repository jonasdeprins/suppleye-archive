//
//  OrdersViewModel.swift
//  Suppleye
//
//  Created by Jonas De Prins on 08/05/2019.
//  Copyright © 2019 Jonas De Prins. All rights reserved.
//

import XCoordinator

protocol OrdersViewModel {
    var count: Int { get }
    func getOrder(at indexPath: IndexPath) -> Order
    func createNew()
}

class OrdersViewModelImpl: OrdersViewModel {
    
    private let orderService: OrderService
    private let router: AnyRouter<OrdersRoute>
    
    init(orderService: OrderService, router: AnyRouter<OrdersRoute>) {
        self.orderService = orderService
        self.router = router
    }
    
    var count: Int {
        return self.orderService.getAll().count
    }
    
    func getOrder(at indexPath: IndexPath) -> Order {
        return self.orderService.getAll()[indexPath.row]
    }
    
    func createNew() {
        self.router.trigger(.new)
    }
    
}
