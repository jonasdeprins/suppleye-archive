//
//  OrderService.swift
//  Suppleye
//
//  Created by Jonas De Prins on 07/05/2019.
//  Copyright © 2019 Jonas De Prins. All rights reserved.
//

import Foundation

protocol OrderService {
    func getAll() -> [Order]
}
