//
//  Order.swift
//  Suppleye
//
//  Created by Jonas De Prins on 08/10/2018.
//  Copyright © 2018 Jonas De Prins. All rights reserved.
//

import Foundation

struct Order: Object {

    let identifier: Identifier
    let date: Date
    var productOrders: [ProductOrder] = []
    
    init(date: Date) {
        self.date = date
        self.identifier = UUID().uuidString
    }
}

class ProductOrder: Object {
    
    let identifier: Identifier
    let product: Product
    private(set) var count: Int
    
    var unitCount: String {
        if count == 1 {
            return "\(count) \(product.unit.single)"
        }
        return "\(count) \(product.unit.plural)"
    }
    
    init(product: Product) {
        self.product = product
        self.identifier = UUID().uuidString
        self.count = product.maxCount
    }
    
    func addQuantity() {
        self.count += 1
    }
    
    func substractQuantity() {
        guard self.count > 0 else { return }
        self.count -= 1
    }
}
