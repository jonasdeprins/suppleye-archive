//
//  OrdersViewController.swift
//  Suppleye
//
//  Created by Jonas De Prins on 08/10/2018.
//  Copyright © 2018 Jonas De Prins. All rights reserved.
//

import Foundation
import UIKit

class OrdersViewController: UITableViewController {
    
    var viewModel: OrdersViewModel!
    
    override var tabBarItem: UITabBarItem! {
        get { return UITabBarItem(title: .localize(.ordersTitle), image: #imageLiteral(resourceName: "trolly"), tag: 0) }
        set {}
    }
    
    override var navigationItem: UINavigationItem {
        let item = UINavigationItem(title: .localize(.ordersTitle))
        item.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(self.didTapAdd))
        return item
    }
    
    override func viewDidLoad() {
        self.view.backgroundColor = .white
        super.viewDidLoad()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let order = self.viewModel.getOrder(at: indexPath)
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") ?? UITableViewCell(frame: .zero)
        let formatter = DateFormatter()
        formatter.dateFormat = "d MMMM yyyy"
        cell.textLabel?.text = formatter.string(from: order.date)
        cell.accessoryType = .disclosureIndicator
        return cell
    }
    
    @objc
    private func didTapAdd(sender: UIBarButtonItem) {
        self.viewModel.createNew()
    }
    
}
