//
//  Product.swift
//  Suppleye
//
//  Created by Jonas De Prins on 08/10/2018.
//  Copyright © 2018 Jonas De Prins. All rights reserved.
//

import Foundation
import UIKit

enum Unit {
    case crate, box
    
    var single: String {
        switch self {
        case .crate:
            return "bak"
        case .box:
            return "doos"
        }
    }
    
    var plural: String {
        switch self {
        case .crate:
            return "bakken"
        case .box:
            return "dozen"
        }
    }
}

class Product: Object {
    
    let identifier: Identifier
    let image: UIImage
    let title: String
    let maxCount: Int
    let unit: Unit
    
    init(image: UIImage, title: String, count: Int, unit: Unit = .crate) {
        self.image = image
        self.title = title
        self.maxCount = count
        self.unit = unit
        self.identifier = UUID().uuidString
    }
}

extension Product: Equatable {
    
    static func == (lhs: Product, rhs: Product) -> Bool {
        return lhs.title == rhs.title
    }
    
}
