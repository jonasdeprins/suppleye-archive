//
//  Object.swift
//  Suppleye
//
//  Created by Jonas De Prins on 13/05/2019.
//  Copyright © 2019 Jonas De Prins. All rights reserved.
//

typealias Identifier = String

protocol Object {

    var identifier: Identifier { get }
}
