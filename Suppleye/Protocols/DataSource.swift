//
//  DataSource.swift
//  Suppleye
//
//  Created by Jonas De Prins on 13/05/2019.
//  Copyright © 2019 Jonas De Prins. All rights reserved.
//

protocol DataSource {

    associatedtype Model

    func getAll() -> [Model]
    func get(by identifier: Identifier) -> Model?
    func create(_ object: Model)
    func update(_ object: Model)
    func delete(_ object: Model)
}

struct AnyDataSource<ModelType>: DataSource {

    private let _getAll: () -> [ModelType]
    private let _getById: (String) -> ModelType?
    private let _create: (ModelType) -> Void
    private let _update: (ModelType) -> Void
    private let _delete: (ModelType) -> Void
    
    init<T: DataSource>(_ dataSource: T) where T.Model == ModelType {
        self._getAll = dataSource.getAll
        self._getById = dataSource.get
        self._create = dataSource.create
        self._update = dataSource.update
        self._delete = dataSource.delete
    }
    
    func getAll() -> [ModelType] {
        return self._getAll()
    }
    
    func get(by identifier: Identifier) -> ModelType? {
        return self._getById(identifier)
    }
    
    func create(_ object: ModelType) {
        self._create(object)
    }

    func update(_ object: ModelType) {
        self._update(object)
    }

    func delete(_ object: ModelType) {
        self._delete(object)
    }
}
