//
//  ActivityViewController.swift
//  Suppleye
//
//  Created by Jonas De Prins on 16/10/2018.
//  Copyright © 2018 Jonas De Prins. All rights reserved.
//

import Foundation
import UIKit
import MessageUI

class ActivityViewController: UIActivityViewController {

    convenience init(productOrders: [ProductOrder], onCompleted: @escaping () -> Void) {
        let shareItem = productOrders.filter { $0.count > 0 }.map {
            "\($0.product.title): \($0.unitCount)"
        }.joined(separator: "\n")
        self.init(activityItems: [shareItem], applicationActivities: [MailActivity(onComplete: onCompleted)])
    }
}

class MailActivity: UIActivity {
    
    private var order: String?
    private let onComplete: () -> Void
    
    init(onComplete: @escaping () -> Void) {
        self.onComplete = onComplete
        super.init()
    }
    
    override var activityViewController: UIViewController? {
        guard let order = self.order else { return nil }

        let dateFormatter = { () -> DateFormatter in
            let df = DateFormatter()
            df.dateFormat = "d/M"
            return df
        }()
        let today = dateFormatter.string(from: Date())
        
        let mail = MFMailComposeViewController()
        mail.setToRecipients(["bvbakoperbrau@proximus.be"])
        mail.setSubject("Bestelling JH De Faar \(today)")
        mail.setMessageBody(order, isHTML: false)
        
        mail.mailComposeDelegate = self
        
        return mail
    }
    
    override var activityType: UIActivity.ActivityType? {
        return .mail
    }
    
    override var activityTitle: String? {
        return .localize(.activityMailSupplier)
    }
    
    override func canPerform(withActivityItems activityItems: [Any]) -> Bool {
        guard MFMailComposeViewController.canSendMail() else { return false }
        return (activityItems.last as? String) != nil
    }
    
    override func prepare(withActivityItems activityItems: [Any]) {
        guard let order = activityItems.last as? String else {
            return
        }
        self.order = order
    }
    
    override var activityImage: UIImage? {
        return #imageLiteral(resourceName: "supplier")
    }
}

extension MailActivity: MFMailComposeViewControllerDelegate {
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        self.activityDidFinish(true)
        if result == .sent {
            self.onComplete()
        }
    }
}
