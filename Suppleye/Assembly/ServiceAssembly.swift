//
//  Assembly.swift
//  Suppleye
//
//  Created by Jonas De Prins on 07/05/2019.
//  Copyright © 2019 Jonas De Prins. All rights reserved.
//

import Swinject
import XCoordinator

class ServiceAssembly: Assembly {
    
    func assemble(container: Swinject.Container) {
        container.register(ProductService.self, factory: self.createProductService).inObjectScope(.container)
        container.register(OrderService.self, factory: self.createOrderService).inObjectScope(.container)
    }
    
    func createProductService(resolver: Resolver) -> ProductService {
        return ProductServiceImpl(productDataSource: resolver.resolve(ProductDataSource.self)!)
    }
    
    func createOrderService(resolver: Resolver) -> OrderService {
        return OrderServiceImpl()
    }
    
}
