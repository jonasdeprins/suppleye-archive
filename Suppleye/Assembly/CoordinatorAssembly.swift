//
//  CoordinatorAssembly.swift
//  Suppleye
//
//  Created by Jonas De Prins on 07/05/2019.
//  Copyright © 2019 Jonas De Prins. All rights reserved.
//

import Swinject
import XCoordinator

class CoordinatorAssembly: Assembly {
    
    func assemble(container: Swinject.Container) {
        container.register(AppCoordinator.self, factory: self.createAppCoordinator)
        container.register(OrdersCoordinator.self, factory: self.createOrdersCoordinator)
        container.register(OrderCoordinator.self, factory: self.createOrderCoordinator)
        container.register(SettingsCoordinator.self, factory: self.createSettingsCoordinator)
    }
    
    func createAppCoordinator(resolver: Resolver) -> AppCoordinator {
        return AppCoordinator(
            ordersCoordinator: resolver.resolve(OrdersCoordinator.self)!,
            settingsCoordinator: resolver.resolve(SettingsCoordinator.self)!
        )
    }
    
    func createOrdersCoordinator(resolver: Resolver) -> OrdersCoordinator {
        return OrdersCoordinator(vcProvider: resolver.resolve(ViewControllerProvider.self)!)
    }
    
    func createOrderCoordinator(resolver: Resolver) -> OrderCoordinator {
        return OrderCoordinator(
            vcProvider: resolver.resolve(ViewControllerProvider.self)!,
            productService: resolver.resolve(ProductService.self)!
        )
    }
    
    func createSettingsCoordinator(resolver: Resolver) -> SettingsCoordinator {
        return SettingsCoordinator(vcProvider: resolver.resolve(ViewControllerProvider.self)!)
    }
}
