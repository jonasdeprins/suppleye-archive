//
//  DataSourceAssembly.swift
//  Suppleye
//
//  Created by Jonas De Prins on 14/05/2019.
//  Copyright © 2019 Jonas De Prins. All rights reserved.
//

import Swinject

class DataSourceAssembly: Assembly {
    
    func assemble(container: Container) {
        container.register(ProductDataSource.self, factory: self.createProductDataSource)
    }
    
    func createProductDataSource(resolver: Resolver) -> ProductDataSource {
        return AnyDataSource(ProductDataSourceStub())
    }
    
}
