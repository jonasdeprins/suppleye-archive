//
//  ViewModelAssembly.swift
//  Suppleye
//
//  Created by Jonas De Prins on 07/05/2019.
//  Copyright © 2019 Jonas De Prins. All rights reserved.
//

import Swinject
import XCoordinator

class ViewModelAssembly: Assembly {
    
    func assemble(container: Swinject.Container) {
        container.register(ProductsViewModel.self, factory: self.createProductsViewModel)
        container.register(OrdersViewModel.self, factory: self.createOrdersViewModel)
        container.register(SettingsViewModel.self, factory: self.createSettingsViewModel)
    }
    
    func createProductsViewModel(resolver: Resolver, productOrders: [ProductOrder], router: AnyRouter<OrderRoute>) -> ProductsViewModel {
        return ProductsViewModelImpl(productOrders: productOrders, router: router)
    }
    
    func createOrdersViewModel(resolver: Resolver, router: AnyRouter<OrdersRoute>) -> OrdersViewModel {
        return OrdersViewModelImpl(orderService: resolver.resolve(OrderService.self)!, router: router)
    }
    
    func createSettingsViewModel(resolver: Resolver, router: AnyRouter<SettingsRoute>) -> SettingsViewModel {
        return SettingsViewModelImpl(router: router, productService: resolver.resolve(ProductService.self)!)
    }
}
