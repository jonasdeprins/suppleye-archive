//
//  ViewControllerAssembly.swift
//  Suppleye
//
//  Created by Jonas De Prins on 07/05/2019.
//  Copyright © 2019 Jonas De Prins. All rights reserved.
//

import Swinject
import XCoordinator

class ViewControllerAssembly: Assembly {
    
    func assemble(container: Swinject.Container) {
        container.register(ViewControllerProvider.self, factory: self.createViewControllerProvider)

        container.register(OrdersViewController.self, factory: self.createOrdersViewController)
        container.register(ProductsViewController.self, factory: self.createProductsViewController)
        container.register(PageViewController.self, factory: self.createPageViewController)
        
        container.register(SettingsViewController.self, factory: self.createSettingsViewController)
        container.register(SettingsProductsViewController.self, factory: self.createSettingsProductsViewController)
        container.register(ProductFormViewController.self, factory: self.createProductFormViewController)
    }
    
    func createViewControllerProvider(resolver: Resolver) -> ViewControllerProvider {
        return ViewControllerProviderImpl(resolver: resolver)
    }
    
    func createOrdersViewController(resolver: Resolver, router: AnyRouter<OrdersRoute>) -> OrdersViewController {
        let viewController = OrdersViewController()
        viewController.viewModel = resolver.resolve(OrdersViewModel.self, argument: router)!
        return viewController
    }
    
    func createProductsViewController(resolver: Resolver, productOrders: [ProductOrder], router: AnyRouter<OrderRoute>) -> ProductsViewController {
        let viewController = ProductsViewController()
        viewController.viewModel = resolver.resolve(ProductsViewModel.self, arguments: productOrders, router)!
        return viewController
    }
    
    func createPageViewController(resolver: Resolver, index: Int, productOrders: [ProductOrder]) -> PageViewController {
        let pages = productOrders.enumerated().map {
             CounterViewController(productOrder: $1, index: $0)
        }
        return PageViewController(productIndex: index, productViewControllers: pages)
    }
    
    func createSettingsViewController(resolver: Resolver, router: AnyRouter<SettingsRoute>) -> SettingsViewController {
        let viewController = SettingsViewController(style: .grouped)
        viewController.viewModel = resolver.resolve(SettingsViewModel.self, argument: router)!
        return viewController
    }
    
    func createSettingsProductsViewController(resolver: Resolver, router: AnyRouter<SettingsRoute>) -> SettingsProductsViewController {
        let viewController = SettingsProductsViewController()
        viewController.viewModel = resolver.resolve(SettingsViewModel.self, argument: router)!
        return viewController
    }
    
    func createProductFormViewController(resolver: Resolver, router: AnyRouter<SettingsRoute>) -> ProductFormViewController {
        let viewController = ProductFormViewController()
        viewController.viewModel = resolver.resolve(SettingsViewModel.self, argument: router)!
        return viewController
    }
}
