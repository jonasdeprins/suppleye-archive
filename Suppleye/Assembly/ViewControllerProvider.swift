//
//  ViewControllerProvider.swift
//  Suppleye
//
//  Created by Jonas De Prins on 21/05/2019.
//  Copyright © 2019 Jonas De Prins. All rights reserved.
//

import UIKit
import Swinject
import XCoordinator

protocol ViewControllerProvider {
    func get<T: UIViewController>(a type: T.Type) -> T
    func get<T: UIViewController, U: Route>(a type: T.Type, router: AnyRouter<U>) -> T
    func get<T: UIViewController, U>(a type: T.Type, argument: U) -> T
    func get<T: UIViewController, U, V>(a type: T.Type, argument: U, _ arg2: V) -> T
    func getCoordinator<T: Coordinator>(of type: T.Type) -> T
}

class ViewControllerProviderImpl: ViewControllerProvider {
    
    private let resolver: Resolver
    
    init(resolver: Resolver) {
        self.resolver = resolver
    }
    
    func get<T>(a type: T.Type) -> T where T: UIViewController {
        return resolver.resolve(type)!
    }
    
    func get<T, U>(a type: T.Type, router: AnyRouter<U>) -> T where T: UIViewController, U: Route {
        return resolver.resolve(type, argument: router)!
    }
    
    func get<T, U>(a type: T.Type, argument: U) -> T where T : UIViewController {
        return resolver.resolve(type, argument: argument)!
    }
    
    func get<T, U, V>(a type: T.Type, argument: U, _ arg2: V) -> T where T : UIViewController {
        return resolver.resolve(type, arguments: argument, arg2)!
    }
    
    func getCoordinator<T>(of type: T.Type) -> T where T : Coordinator {
        return resolver.resolve(type)!
    }
}
