//
//  ProductCell.swift
//  Suppleye
//
//  Created by Jonas De Prins on 08/10/2018.
//  Copyright © 2018 Jonas De Prins. All rights reserved.
//

import Foundation
import UIKit

class ProductCell: UICollectionViewCell {
    
    @IBOutlet private weak var logoImageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    
    @IBOutlet private weak var countView: UIView!
    @IBOutlet private weak var countLabel: UILabel!
    
    static var reuseIdentifier: String {
        return "\(self)"
    }
    
    static var nib: UINib {
        return UINib(nibName: "ProductCell", bundle: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.countView.layer.cornerRadius = countView.frame.height / 2
    }
    
    func setup(productOrder: ProductOrder) {
        self.logoImageView.image = productOrder.product.image
        self.titleLabel.text = productOrder.product.title
        self.countView.isHidden = productOrder.count == 0
        self.countLabel.text = "\(productOrder.count)"
    }
    
}
