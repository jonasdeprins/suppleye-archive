//
//  PageViewController.swift
//  Suppleye
//
//  Created by Jonas De Prins on 08/10/2018.
//  Copyright © 2018 Jonas De Prins. All rights reserved.
//

import Foundation
import UIKit

class PageViewController: UIPageViewController {
    
    private let productIndex: Int
    private let productViewControllers: [CounterViewController]

    init(productIndex: Int, productViewControllers: [CounterViewController]) {
        self.productIndex = productIndex
        self.productViewControllers = productViewControllers
        super.init(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
    }
    
    override func viewDidLoad() {
        self.dataSource = self
        self.delegate = self
        
        let firstVC = self.productViewControllers[self.productIndex]
        self.setViewControllers([firstVC], direction: .forward, animated: true, completion: nil)
        self.setTitle()
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.barStyle = .black
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.barStyle = .default
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.barStyle = .default
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setTitle() {
        self.title = self.viewControllers?.first?.title
    }
}

extension PageViewController: UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let index = (viewController as? CounterViewController)?.index else { return nil }
        guard index < self.productViewControllers.endIndex - 1 else {
            return nil
        }
        return self.productViewControllers[index + 1]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let index = (viewController as? CounterViewController)?.index else { return nil }
        guard index > 0 else {
            return nil
        }
        return self.productViewControllers[index - 1]
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return self.productViewControllers.count
    }
    
}

extension PageViewController: UIPageViewControllerDelegate {
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        self.setTitle()
    }
    
}
