//
//  CounterViewController.swift
//  Suppleye
//
//  Created by Jonas De Prins on 10/10/2018.
//  Copyright © 2018 Jonas De Prins. All rights reserved.
//

import Foundation
import UIKit

class CounterViewController: UIViewController {
    
    private let productOrder: ProductOrder
    let index: Int
    
    @IBOutlet weak var backgroundImageView: UIImageView!

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var countTetField: UITextField!
    @IBOutlet weak var visualEffectViewAdd: UIVisualEffectView! {
        didSet {
            visualEffectViewAdd.layer.cornerRadius = 50
            visualEffectViewAdd.clipsToBounds = true
        }
    }
    @IBOutlet weak var visualEffectViewSubscract: UIVisualEffectView! {
        didSet {
            visualEffectViewSubscract.layer.cornerRadius = 50
            visualEffectViewSubscract.clipsToBounds = true
        }
    }
    
    init(productOrder: ProductOrder, index: Int) {
        self.productOrder = productOrder
        self.index = index
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = self.productOrder.product.title
        self.backgroundImageView.image = self.productOrder.product.image
        self.imageView.image = self.productOrder.product.image
        self.view.bringSubviewToFront(self.countTetField)
        self.setCountLabel()
    }
    
    func setCountLabel() {
        self.countTetField.text = "\(self.productOrder.count)"
    }

    @IBAction func handleAddition(_ sender: Any) {
        UIImpactFeedbackGenerator(style: .light).impactOccurred()
        self.productOrder.addQuantity()
        self.setCountLabel()
    }
    
    @IBAction func handleSubscration(_ sender: Any) {
        UIImpactFeedbackGenerator(style: .light).impactOccurred()
        self.productOrder.substractQuantity()
        self.setCountLabel()
    }
    
    @IBAction func didTapTextField(_ sender: Any) {
        countTetField.text = ""
    }
    
    @IBAction func editingDidChange(_ sender: Any) {
        if let count = Int(countTetField.text ?? "0") {
//            self.productOrder.countManual = count
            self.setCountLabel()
        }
    }
}
