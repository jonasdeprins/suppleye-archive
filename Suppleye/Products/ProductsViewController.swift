//
//  ProductsViewController.swift
//  Suppleye
//
//  Created by Jonas De Prins on 08/10/2018.
//  Copyright © 2018 Jonas De Prins. All rights reserved.
//

import Foundation
import UIKit

class ProductsViewController: UIViewController {
    
    var viewModel: ProductsViewModel!

    private enum ViewState: Int {
        case tile = 0, list
    }
    private var state: ViewState = .tile
    
    
    @IBOutlet private weak var collectionView: UICollectionView!
    
    override var navigationItem: UINavigationItem {
        let item = UINavigationItem(title: .localize(.productsTitle))
        item.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancel))
        item.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(action))
        item.largeTitleDisplayMode = .always
        return item
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView.backgroundColor = .white
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.collectionView?.contentInsetAdjustmentBehavior = .always
        self.collectionView.register(ProductCell.nib, forCellWithReuseIdentifier: ProductCell.reuseIdentifier)
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        if #available(iOS 13.0, *) {
            self.isModalInPresentation = true
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.setView(state: self.state)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.collectionView.reloadData()
    }

    @IBAction func didChangeViewState(_ sender: UISegmentedControl) {
        guard let viewState = ViewState(rawValue: sender.selectedSegmentIndex) else { return }
        self.setView(state: viewState)
    }
    
    private func setView(state: ViewState) {
        self.state = state
        let width = collectionView.safeAreaLayoutGuide.layoutFrame.width
        let size = state == .list ?
            CGSize(width: width, height: 60) :
            CGSize(width: width/2 - 10, height: width/2 - 10)
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = size
        layout.minimumLineSpacing = state == .list ? 1 : 10
        layout.minimumInteritemSpacing = state == .list ? 0 : 10
        UIView.animateKeyframes(withDuration: 0.3, delay: 0, options: [.layoutSubviews], animations: {
            self.collectionView.collectionViewLayout = layout
            self.collectionView.backgroundColor = state == .list ? UIColor(white: 0.83, alpha: 1) : .white
        })
    }
    
    @objc
    private func cancel(sender: UIBarButtonItem) {
        self.viewModel.cancel()
    }
    
    @objc
    private func action(sender: UIBarButtonItem) {
        self.viewModel.action()
    }
}

extension ProductsViewController: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewModel.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductCell.reuseIdentifier, for: indexPath) as? ProductCell
        let productOrder = self.viewModel.productOrder(at: indexPath)
        cell?.setup(productOrder: productOrder)
        return cell ?? UICollectionViewCell(frame: .zero)
    }
}

extension ProductsViewController: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.viewModel.showDetails(at: indexPath.row)
    }
}
