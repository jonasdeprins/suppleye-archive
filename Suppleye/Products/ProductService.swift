//
//  ProductService.swift
//  Suppleye
//
//  Created by Jonas De Prins on 07/05/2019.
//  Copyright © 2019 Jonas De Prins. All rights reserved.
//

import Foundation

protocol ProductService {
    func getAll() -> [Product]
    func getCount() -> Int
    func get(at indexPath: IndexPath) -> Product?
    func save(product: Product)
    func getProductOrders() -> [ProductOrder]
}
