//
//  ProductServiceImpl.swift
//  Suppleye
//
//  Created by Jonas De Prins on 07/05/2019.
//  Copyright © 2019 Jonas De Prins. All rights reserved.
//

import Foundation

class ProductServiceImpl: ProductService {
    
    private let productDataSource: ProductDataSource
    
    init(productDataSource: ProductDataSource) {
        self.productDataSource = productDataSource
    }

    func getAll() -> [Product] {
        return productDataSource.getAll()
    }
    
    func getCount() -> Int {
        return (self.productDataSource.getAll() as [Product]).count
    }

    func get(at indexPath: IndexPath) -> Product? {
        return self.productDataSource.getAll()[indexPath.row]
    }

    func save(product: Product) {
        self.productDataSource.create(product)
    }

    func getProductOrders() -> [ProductOrder] {
        return self.productDataSource.getAll().map { ProductOrder(product: $0) }
    }
}
