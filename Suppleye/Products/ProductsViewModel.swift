//
//  ProductsViewModel.swift
//  Suppleye
//
//  Created by Jonas De Prins on 07/05/2019.
//  Copyright © 2019 Jonas De Prins. All rights reserved.
//

import XCoordinator

protocol ProductsViewModel {
    var productOrders: [ProductOrder] { get }
    var count: Int { get }
    func productOrder(at indexPath: IndexPath) -> ProductOrder
    func cancel()
    func action()
    func showDetails(at index: Int)
}

class ProductsViewModelImpl: ProductsViewModel {

    let productOrders: [ProductOrder]
    private let router: AnyRouter<OrderRoute>
    
    init(productOrders: [ProductOrder], router: AnyRouter<OrderRoute>) {
        self.productOrders = productOrders
        self.router = router
    }
    
    var count: Int {
        return productOrders.count
    }

    func productOrder(at indexPath: IndexPath) -> ProductOrder {
        return productOrders[indexPath.row]
    }
    
    func showDetails(at index: Int) {
        self.router.trigger(.productDetails(index: index))
    }
    
    func cancel() {
        self.router.trigger(.cancel)
    }
    
    func action() {
        self.router.trigger(.action)
    }
}
