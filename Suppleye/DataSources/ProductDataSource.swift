//
//  ProductDataSource.swift
//  Suppleye
//
//  Created by Jonas De Prins on 13/05/2019.
//  Copyright © 2019 Jonas De Prins. All rights reserved.
//

import Foundation

typealias ProductDataSource = AnyDataSource<Product>

class ProductDataSourceImpl: DataSource {
    
    func getAll() -> [Product] {
        return []
    }
    
    func get(by identifier: Identifier) -> Product? {
        return nil
    }
    
    func create(_ object: Product) {}
    func update(_ object: Product) {}
    func delete(_ object: Product) {}
}

class ProductDataSourceStub: DataSource {
    
    private var products: [Product] = [
        Product(image: #imageLiteral(resourceName: "primus-1"), title: "Primus", count: 15),
        Product(image: #imageLiteral(resourceName: "val-plat"), title: "Val plat", count: 3),
        Product(image: #imageLiteral(resourceName: "val-bruis"), title: "Val bruis", count: 3),
        Product(image: #imageLiteral(resourceName: "pepsi"), title: "Pepsi", count: 3),
        Product(image: #imageLiteral(resourceName: "pepsi-max"), title: "Pepsi Max", count: 3),
        Product(image: #imageLiteral(resourceName: "icetea-green"), title: "Ice Tea Green", count: 3),
        Product(image: #imageLiteral(resourceName: "val-orange"), title: "Val Orange", count: 3),
        Product(image: #imageLiteral(resourceName: "val-lime"), title: "Val Lime", count: 3),
        Product(image: #imageLiteral(resourceName: "val-tea"), title: "Val Tea", count: 3),
        Product(image: #imageLiteral(resourceName: "looza-ace"), title: "Looza ACE", count: 3),
        Product(image: #imageLiteral(resourceName: "duvel"), title: "Duvel", count: 5),
        Product(image: #imageLiteral(resourceName: "rouge"), title: "Kasteelbier Rouge", count: 5),
        Product(image: #imageLiteral(resourceName: "mystic"), title: "Kriek Mystic", count: 3),
        Product(image: #imageLiteral(resourceName: "strongbow-apple"), title: "Strongbow Apple", count: 5, unit: .box),
        Product(image: #imageLiteral(resourceName: "stroingbow-elderflower"), title: "Strongbow Elderflower", count: 5, unit: .box),
        Product(image: #imageLiteral(resourceName: "strongbow-redberry"), title: "Strongbow Red Berry", count: 5, unit: .box),
        Product(image: #imageLiteral(resourceName: "croky-zout"), title: "Croky Zout", count: 2, unit: .box),
        Product(image: #imageLiteral(resourceName: "croky-paprika"), title: "Croky Paprika", count: 2, unit: .box),
        Product(image: #imageLiteral(resourceName: "croky-pickles"), title: "Crocky Pickles", count: 2, unit: .box),
        Product(image: #imageLiteral(resourceName: "beker"), title: "Bekers", count: 1, unit: .box)
    ]
    
    func getAll() -> [Product] {
        return self.products
    }
    
    func get(by identifier: Identifier) -> Product? {
        return self.products.first(where: { $0.identifier == identifier })
    }
    
    func create(_ object: Product) {
        self.products.append(object)
    }
    
    func update(_ object: Product) {}
    
    func delete(_ object: Product) {
        self.products = products.drop { $0.identifier == object.identifier }.map { $0 }
    }
}

