//
//  OrdersCoordinator.swift
//  Suppleye
//
//  Created by Jonas De Prins on 07/05/2019.
//  Copyright © 2019 Jonas De Prins. All rights reserved.
//

import XCoordinator

enum OrdersRoute: Route {
    case orders
    case new
}

class OrdersCoordinator: NavigationCoordinator<OrdersRoute> {
    
    private let viewControllerProvider: ViewControllerProvider
    
    init(vcProvider: ViewControllerProvider) {
        self.viewControllerProvider = vcProvider
        super.init(initialRoute: .orders)
    }
    
    override func prepareTransition(for route: OrdersRoute) -> NavigationTransition {
        switch route {
        case .orders:
            return .push(self.viewControllerProvider.get(a: OrdersViewController.self, router: self.anyRouter))
        case .new:
            return .present(self.viewControllerProvider.getCoordinator(of: OrderCoordinator.self))
        }
    }
    
}
