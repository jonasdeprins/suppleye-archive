//
//  OrderCoordinator.swift
//  Suppleye
//
//  Created by Jonas De Prins on 07/05/2019.
//  Copyright © 2019 Jonas De Prins. All rights reserved.
//

import XCoordinator

enum OrderRoute: Route {
    case products
    case productDetails(index: Int)
    case cancel
    case action
    case finish
}

class OrderCoordinator: NavigationCoordinator<OrderRoute> {

    private let viewControllerProvider: ViewControllerProvider
    
    private let productOrders: [ProductOrder]
    
    init(vcProvider: ViewControllerProvider, productService: ProductService) {
        self.viewControllerProvider = vcProvider
        self.productOrders = productService.getProductOrders()
        super.init(initialRoute: .products)
    }

    override func prepareTransition(for route: OrderCoordinator.RouteType) -> NavigationTransition {
        switch route {
        case .products:
            let viewController = self.viewControllerProvider.get(a: ProductsViewController.self, argument: self.productOrders, self.anyRouter)
            return .push(viewController)
        case let .productDetails(index):
            let viewController = self.viewControllerProvider.get(a: PageViewController.self, argument: index, self.productOrders)
            return .push(viewController)
        case .cancel:
            return .dismiss()
        case .action:
            return .present(ActivityViewController(productOrders: self.productOrders, onCompleted: { [weak self] in
                self?.anyRouter.trigger(.finish)
            }))
        case .finish:
            return .dismissToRoot()
        }
    }
}
