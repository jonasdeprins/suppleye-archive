//
//  SettingsCoordinator.swift
//  Suppleye
//
//  Created by Jonas De Prins on 22/05/2019.
//  Copyright © 2019 Jonas De Prins. All rights reserved.
//

import XCoordinator

enum SettingsRoute: Route {
    case main
    case products
    case addProduct
}

class SettingsCoordinator: NavigationCoordinator<SettingsRoute> {
    
    private let viewControllerProvider: ViewControllerProvider
    
    init(vcProvider: ViewControllerProvider) {
        self.viewControllerProvider = vcProvider
        super.init(initialRoute: .main)
    }
    
    override func prepareTransition(for route: SettingsRoute) -> NavigationTransition {
        switch route {
        case .main:
            return .push(self.viewControllerProvider.get(a: SettingsViewController.self, router: self.anyRouter))
        case .products:
            return .push(self.viewControllerProvider.get(a: SettingsProductsViewController.self, router: self.anyRouter))
        case .addProduct:
            return .present(self.viewControllerProvider.get(a: ProductFormViewController.self, router: self.anyRouter))
        }
    }
    
}
