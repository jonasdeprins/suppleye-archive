//
//  AppCoordinator.swift
//  Suppleye
//
//  Created by Jonas De Prins on 07/05/2019.
//  Copyright © 2019 Jonas De Prins. All rights reserved.
//

import XCoordinator

enum AppRoute: Route {
    case orders
    case settings
}

class AppCoordinator: TabBarCoordinator<AppRoute> {
    
    private let ordersCoordinator: OrdersCoordinator
    private let settingsCoordinator: SettingsCoordinator
    
    init(ordersCoordinator: OrdersCoordinator, settingsCoordinator: SettingsCoordinator) {
        self.ordersCoordinator = ordersCoordinator
        self.settingsCoordinator = settingsCoordinator
        
        super.init(tabs: [
            self.ordersCoordinator.anyRouter,
            self.settingsCoordinator.anyRouter
        ], select: 0)
    }
    
    override func prepareTransition(for route: AppRoute) -> TabBarTransition {
        switch route {
        case .orders:
            return .select(self.ordersCoordinator)
        case .settings:
            return .select(self.settingsCoordinator)
        }
    }
    
}

