//
//  StringsExtensions.swift
//  Suppleye
//
//  Created by Jonas De Prins on 17/05/2019.
//  Copyright © 2019 Jonas De Prins. All rights reserved.
//

import Foundation

extension String {
    
    static func localize(_ l10n: L10n, arguments: CVarArg...) -> String {
        return String(format: l10n.localize, arguments: arguments)
    }
}
