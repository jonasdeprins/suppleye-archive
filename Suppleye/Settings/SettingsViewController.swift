//
//  SettingsViewController.swift
//  Suppleye
//
//  Created by Jonas De Prins on 07/05/2019.
//  Copyright © 2019 Jonas De Prins. All rights reserved.
//

import UIKit
import Eureka

class SettingsViewController: FormViewController {
    
    var viewModel: SettingsViewModel!
    
    override var tabBarItem: UITabBarItem! {
        get { return UITabBarItem(title: .localize(.settingsTitle), image: #imageLiteral(resourceName: "settings"), tag: 1) }
        set {}
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = .localize(.settingsTitle)
        self.setupView()
    }
    
    private func setupView() {
        self.form +++ Section(.localize(.settingsSupplierTitle)) { section in
                section.footer = HeaderFooterView(title: .localize(.settingsSupplierFooter, arguments: "test"))
            }
            <<< EmailRow() { row in
                row.title = .localize(.settingsSupplierEmail)
            }
            <<< TextRow() { row in
                row.title = .localize(.settingsSupplierSubject)
                row.placeholder = .localize(.settingsSupplierSubjectPlaceholder)
            }
        
        self.form +++ Section(.localize(.settingsViewTitle))
            <<< SegmentedRow<UIImage>() { row in
                row.title = .localize(.settingsViewProducts)
                row.options = [#imageLiteral(resourceName: "collection"), #imageLiteral(resourceName: "list")]
                row.onChange({ row in
                    let title: String = row.value == #imageLiteral(resourceName: "collection") ?
                    .localize(.settingsViewProductsTile) :
                    .localize(.settingsViewProductsList)
                    row.title = .localize(.settingsViewProducts, arguments: title)
                    row.updateCell()
                })
            }
        
        self.form +++ Section(.localize(.settingsProductsTitle))
            <<< LabelRow() { row in
                row.title = .localize(.settingsProductsManage)
                row.cell.accessoryType = .disclosureIndicator
                row.onCellSelection { [weak self] _, _ in
                    self?.viewModel.productsAction()
                }
            }
            <<< ButtonRow() { row in
                row.title = .localize(.settingsProductsImport)
                row.onCellSelection({ [weak self] _, _ in
                    self?.viewModel.importAction()
                })
            }
            <<< ButtonRow() { row in
                row.title = .localize(.settingsProductsExport)
                row.onCellSelection({ [weak self] _, _ in
                    self?.viewModel.exportAction()
                })
            }
        
    }
    
}
