//
//  SettingsViewModel.swift
//  Suppleye
//
//  Created by Jonas De Prins on 22/05/2019.
//  Copyright © 2019 Jonas De Prins. All rights reserved.
//

import XCoordinator
import Eureka

protocol SettingsViewModel {
    
    var productRows: [BaseRow] { get }
    func productsAction()
    func exportAction()
    func importAction()
    func addProductAction()
}

class SettingsViewModelImpl: SettingsViewModel {
    
    private let router: AnyRouter<SettingsRoute>
    private let productService: ProductService
    
    init(router: AnyRouter<SettingsRoute>, productService: ProductService) {
        self.router = router
        self.productService = productService
    }
    
    var productRows: [BaseRow] {
        return self.productService.getAll().map { product in
            return LabelRow() { row in
                row.title = product.title
                let deleteAction = SwipeAction(style: .destructive, title: nil, handler: { (action, row, onComplete) in
                    onComplete?(true)
                })
                row.trailingSwipe.actions = [deleteAction]
                row.cell.imageView?.image = product.image

                row.value = "\(product.maxCount)"
                row.onCellSelection({ [weak self] (label, row) in
                    self?.router.trigger(.addProduct)
                })
            }
        }
    }
    
    func productsAction() {
        self.router.trigger(.products)
    }
    
    func exportAction() {
        
    }
    
    func importAction() {
        
    }
    
    func addProductAction() {
        self.router.trigger(.addProduct)
    }
    
}

final class SettingsProductRow: Row<SettingsProductCell>, RowType {
    required init(tag: String?) {
        super.init(tag: tag)
        self.cellProvider = CellProvider<SettingsProductCell>(nibName: "SettingsProductCell")
    }
}
