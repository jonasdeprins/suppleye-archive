//
//  SettingsProductCell.swift
//  Suppleye
//
//  Created by Jonas De Prins on 23/05/2019.
//  Copyright © 2019 Jonas De Prins. All rights reserved.
//

import Eureka

class SettingsProductCell: Cell<Product>, CellType {
    
//    @IBOutlet internal override var imageV`xiew: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel!
    
    override func setup() {
        super.setup()
//        self.imageView.
        self.titleLabel.text = self.row.value?.title
        self.countLabel.text = "\(self.row.value?.maxCount)"
    }
    
}
