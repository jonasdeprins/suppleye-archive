//
//  SettingsProductsViewController.swift
//  Suppleye
//
//  Created by Jonas De Prins on 22/05/2019.
//  Copyright © 2019 Jonas De Prins. All rights reserved.
//

import Eureka

class SettingsProductsViewController: FormViewController {
    
    var viewModel: SettingsViewModel!
    
    override var navigationItem: UINavigationItem {
        let item = UINavigationItem()
        item.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(self.addProduct))
        return item
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = .localize(.settingsProductsTitle)
        self.setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView.isEditing = false
    }
    
    private func setupView() {
        self.form +++ MultivaluedSection(multivaluedOptions: [], header: .localize(.settingsProductsTitle), footer: "") { section in
            self.viewModel.productRows.forEach(section.append)
        }
    }
    
    @objc
    private func addProduct() {
        self.tableView.isEditing = true
//        self.viewModel.addProductAction()
    }
}
