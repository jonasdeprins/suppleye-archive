//
//  L10n.swift
//  Suppleye
//
//  Created by Jonas De Prins on 17/05/2019.
//  Copyright © 2019 Jonas De Prins. All rights reserved.
//

import Foundation

enum L10n: String {

    // MARK: Orders
    case ordersTitle = "orders.title"
    
    // MARK: Products
    case productsTitle = "products.title"
    
    // MARK: Settings
    case settingsTitle = "settings.title"
    case settingsSupplierTitle = "settings.supplier.title"
    case settingsSupplierFooter = "settings.supplier.footer"
    case settingsSupplierEmail = "settings.supplier.email"
    case settingsSupplierSubject = "settings.supplier.subject"
    case settingsSupplierSubjectPlaceholder = "settings.supplier.subject.placeholder"
    case settingsViewTitle = "settings.view.title"
    case settingsViewProducts = "settings.view.products"
    case settingsViewProductsTile = "settings.view.products.tile"
    case settingsViewProductsList = "settings.view.products.list"
    case settingsProductsTitle = "settings.products.title"
    case settingsProductsManage = "settings.products.manage"
    case settingsProductsImport = "settings.products.import"
    case settingsProductsExport = "settings.products.export"

    // MARK: Other
    case activityMailSupplier = "activity.mail_supplier"
    
    var localize: String {
        return Bundle.main.localizedString(forKey: self.rawValue, value: nil, table: "Strings")
    }
}
